This app is not meant for a mass market but is crucial for
[WalletScrutiny](https://walletscrutiny.com) to work.

Google allows developers to target languages, countries, devices, ... with
individual versions of their app and there is no public api giving access to all
those variations.

Additionally Google might distribute altered versions of the app without the
provider's knowledge, thanks to App Bundles, Google is pushing for recently: A
scheme under which the developer shares the app signing key with Google.

In order to capture all versions of an app that are being distributed, this open
source software will be designed to:

1. Monitor app install/update events
1. Check if the appId is of interest
1. Hash the new APK
1. Check with a server if the hash is known
1. If the hash is unknown, upload the APK to a server
1. The server unpacks the received APK
1. Determines the appId and version
1. Tries to reproduce the build
1. Publicly shares results
1. Should reproducing the build fail, the appId could be automatically flagged as "under
   investigation", with app users getting notified not to update that app until the issue is
   resolved.

Deterministic Builds
====================

This app will also get the same scrutiny as wallets once it is in Google Play. For now there is only

Build Instructions
==================

with no claim of reproducibility:

```
$ ./gradlew :app:assemble
$ ls app/build/outputs/apk/release/*.apk
app/build/outputs/apk/release/app-release-unsigned.apk
```

