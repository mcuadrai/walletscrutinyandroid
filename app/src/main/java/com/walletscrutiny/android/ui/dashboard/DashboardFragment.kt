package com.walletscrutiny.android.ui.dashboard

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.walletscrutiny.android.R
import java.util.*

class DashboardFragment : Fragment() {
    private lateinit var dashboardViewModel: DashboardViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        dashboardViewModel =
            ViewModelProviders.of(this).get(DashboardViewModel::class.java)
        val root = inflater.inflate(R.layout.fragment_dashboard, container, false)
        val viewManager = LinearLayoutManager(requireContext())
        val viewAdapter = WalletAppsAdapter(Collections.singletonList(AppEntry("bla.foo", "3.3", 33, "Bla Foo", "asdlfjasdlfj")), requireContext())
        val recyclerView = root.findViewById<RecyclerView>(R.id.rvWalletApps).apply {
            // use a linear layout manager
            layoutManager = viewManager

            // specify an viewAdapter (see also next example)
            adapter = viewAdapter
        }

        return root
    }
}

object AppEntryDiffer : DiffUtil.ItemCallback<AppEntry>() {
    override fun areItemsTheSame(oldItem: AppEntry, newItem: AppEntry) = oldItem.appId == newItem.appId

    override fun areContentsTheSame(oldItem: AppEntry, newItem: AppEntry): Boolean {
        return oldItem.appId == newItem.appId && oldItem.versionId == newItem.versionId
    }
}

class WalletAppsAdapter(private val dataset: List<AppEntry>, context: Context) : ListAdapter<AppEntry, RecyclerView.ViewHolder>(AppEntryDiffer) {
    val layoutInflater = LayoutInflater.from(context)
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val view = layoutInflater.inflate(R.layout.app_view, parent, false)
        return AppViewHolder(view)
    }

    override fun getItemCount() = dataset.size

    override fun getItem(position: Int) = dataset[position]

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val appEntry = getItem(position)
        (holder as AppViewHolder).apply {
            appName.text = appEntry.name
        }
    }
}

class AppViewHolder(val view: View): RecyclerView.ViewHolder(view) {
    var ivAppIcon: ImageView = view.findViewById(R.id.ivAppIcon)
    var appName: TextView = view.findViewById(R.id.tvAppName)
}