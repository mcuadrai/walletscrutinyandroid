package com.walletscrutiny.android

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.util.Log
import java.io.*
import java.security.MessageDigest


class UpgradeMonitor : BroadcastReceiver() {
    override fun onReceive(context: Context?, intent: Intent?) {
        Log.d("UpgradeMonitor", "Detected event: $intent")
        when (intent?.action) {
            "android.intent.action.PACKAGE_ADDED",
            "android.intent.action.PACKAGE_CHANGED",
            "android.intent.action.PACKAGE_INSTALL",
            "android.intent.action.PACKAGE_REPLACED"
            -> check(context!!, intent.data!!.schemeSpecificPart!!)
        }
    }

    private fun check(context: Context, appId: String) {
        Log.d("UpgradeMonitor", "Detected event: $appId")

        val mainIntent = Intent(Intent.ACTION_MAIN, null)
        mainIntent.addCategory(Intent.CATEGORY_LAUNCHER)
        val pkgAppsList = context.getPackageManager().queryIntentActivities( mainIntent, 0)
        val info = pkgAppsList.find {
            appId == it.activityInfo.taskAffinity
        }!!
        val hash = sha256(info.activityInfo.applicationInfo.publicSourceDir)
        Log.d("UpgradeMonitor", "Hash is: $hash")
    }

    private fun sha256(fileName: String): String {
        val inputStream = FileInputStream(fileName)
        val buffer = ByteArray(1024)
        val digest: MessageDigest = MessageDigest.getInstance("SHA-256")
        var numRead = 0
        while (numRead != -1) {
            numRead = inputStream.read(buffer)
            if (numRead > 0) {
                digest.update(buffer, 0, numRead)
            }
        }
        return digest.digest().joinToString("") { "%02x".format(it) }
    }
}
