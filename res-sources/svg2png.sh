#!/bin/bash

convert -background none icon.svg -resize 36x36 ../app/src/main/res/drawable-ldpi/icon.png
convert -background none icon.svg -resize 48x48 ../res/drawable-mdpi/icon.png
convert -background none icon.svg -resize 72x72 ../res/drawable-hdpi/icon.png
convert -background none icon.svg -resize 96x96 ../res/drawable-xhdpi/icon.png
convert -background none icon.svg -resize 512x512 uhdpi_icon.png

convert -background none addword.svg -resize 36x36 ../res/drawable-ldpi/addword.png
convert -background none addword.svg -resize 48x48 ../res/drawable-mdpi/addword.png
convert -background none addword.svg -resize 72x72 ../res/drawable-hdpi/addword.png
convert -background none addword.svg -resize 96x96 ../res/drawable-xhdpi/addword.png

convert -background none premium_indicator.svg -resize x36 ../res/drawable-ldpi/premium_indicator.png
convert -background none premium_indicator.svg -resize x48 ../res/drawable-mdpi/premium_indicator.png
convert -background none premium_indicator.svg -resize x72 ../res/drawable-hdpi/premium_indicator.png
convert -background none premium_indicator.svg -resize x96 ../res/drawable-xhdpi/premium_indicator.png

convert -background none featured.svg -resize 1024x500 featured.png
convert -background none ka.svg -resize 256x256 ka.png

for f in add add_frameless affiliation_student expired information observe pen q server_access statistic table transfer_down_up trash user_phd missing_file gear
do
  echo $f
  file="PRZE icon set cc-by-sa/"$f".svg"
  convert -background none "$file" -resize 50x ../res/drawable-ldpi/$f.png
  convert -background none "$file" -resize 67x ../res/drawable-mdpi/$f.png
  convert -background none "$file" -resize 100x ../res/drawable-hdpi/$f.png
  convert -background none "$file" -resize 134x ../res/drawable-xhdpi/$f.png
done

#   uses_quizlet_blue.CpmW.png
#   widget_icon.png
#   grid.png

