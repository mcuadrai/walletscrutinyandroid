package com.walletscrutiny.server

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.dataformat.xml.XmlFactory
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory
import com.fasterxml.jackson.module.kotlin.registerKotlinModule
import java.io.File
import java.io.FileNotFoundException
import java.net.InetSocketAddress
import java.net.Socket
import java.security.MessageDigest
import java.util.*
import java.util.concurrent.TimeUnit
import java.util.logging.Level
import java.util.logging.Logger
import javax.servlet.annotation.WebServlet
import javax.servlet.http.HttpServlet
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse
import kotlin.concurrent.thread

@WebServlet("/apks")
// TODO: limit upload size
// TODO: detect zip bombs
//@MultipartConfig(
//    location="/tmp",
//    fileSizeThreshold=1024*1024,
//    maxFileSize=5*1024*1024,
//    maxRequestSize=7*1024*1024)
class ApksController : HttpServlet() {
    private lateinit var state: State
    private val yamlMapper = ObjectMapper(YAMLFactory()).also { it.registerKotlinModule() }
    private val xmlMapper = ObjectMapper(XmlFactory()).also { it.registerKotlinModule() }
    private var basePath = "/var/lib/walletscrutiny"
    private var dataPath = "$basePath/dat"
    private var ip = "127.0.0.1"
    private val logger = Logger.getLogger(ApksController::class.java.name)

    override fun init() {
        if (!File(dataPath).isDirectory && !File(dataPath).mkdirs()) {
            throw Error("""Can't create folder $dataPath
                Please allow the tomcat user to write to $basePath by creating a file like
                $ cat /etc/systemd/system/tomcat9.service.d/walletscrutiny.conf
                [Service]
                ReadWritePaths=/var/lib/walletscrutiny/
            """.trimIndent())
        }
        Socket().use { socket ->
            socket.connect(InetSocketAddress("google.com", 80))
            ip = socket.localAddress.hostAddress
        }
        state = try {
            yamlMapper.readValue(File("$dataPath/state.yaml"), State::class.java)
        } catch (e: FileNotFoundException) {
            State(0, mutableMapOf())
        }
    }

    override fun doGet(req: HttpServletRequest, res: HttpServletResponse) {
        val appId = req.getParameter("appId")
        val hash = req.getParameter("appHash")
        if (appId != null && hash != null) {
            res.writer.write("""
                What we know about an $appId APK with hash $hash:
                ${state.apps[appId]?.knownBinaries?.get(hash) ?: "no data available"}
            """.trimIndent())
        } else if (appId != null) {
            res.writer.write("""
                Hi!
                
                For AppId "$appId", the following versions are known to us:
                ${state.apps[appId]?.knownBinaries?.values?.map { it.versionName }?.toSortedSet() ?: "none so far"}
            """.trimIndent())
        } else {
            res.writer.write("""
                Hi!
                
                This service can analyze Android APKs.
                
                Apps currently known are [${state.apps.keys.joinToString(", ")}].

                If you want to know if an app is being tracked by this platform,
                provide its appId as such:
                $ wget -q -O- http://$ip:8080/server/apks?appId=some.wallet

                If you want to know, if a specific APK is known, get its sha256sum
                $ sha256sum wallet.apk
                cc616d5c4b67911e3ef65dc58f6cf045d18810826cd362a85776459607cb070c  wallet.apk
                and run:
                $ wget -q -O- http://$ip:8080/server/apks?appId=some.wallet&appHash=cc616d5c4b67911e3ef65dc58f6cf045d18810826cd362a85776459607cb070c
                
                If the apk is not known, please upload it using
                $ curl --request POST --data-binary @'/tmp/wallet.apk' http://$ip:8080/server/apks
            """.trimIndent())
        }
    }

    // curl --request POST --data-binary @path/to/wallet.apk http://localhost:8080/server/apks
    override fun doPost(req: HttpServletRequest, resp: HttpServletResponse) {
        // store
        val nonce = UUID.randomUUID().toString()
        val tmpFilePath = "$dataPath/$nonce.apk"
        req.inputStream.use { input ->
            File(tmpFilePath).outputStream().use {
                input.copyTo(it)
            }
        }
        val hash = sha256(tmpFilePath)
        val reporter = req.getParameter("reporter")
        if (state.apps.values.any { it.knownBinaries[hash] != null }) {
            resp.writer.println("The provided APK was already known.")
        } else {
            resp.writer.println("Thank you $reporter for providing this new APK. We will shortly analyze it.")
            thread {
                processNewApk(tmpFilePath, nonce, hash, req.getParameter("reporter"))
            }
        }
    }

    private fun processNewApk(tmpFilePath: String, nonce: String, hash: String, reporter: String?) {
        // unwrap
        val tmpFolderPath = "$dataPath/$nonce"
        val pb = ProcessBuilder("apktool", "d", "-o", nonce, "$nonce.apk")
        pb.inheritIO()
        pb.directory(File(dataPath))
        val process = pb.start()
        if (!process.waitFor(1, TimeUnit.MINUTES)) {
            File(tmpFolderPath).deleteRecursively()
            File(tmpFilePath).delete()
            return
        }
        if (!File("$tmpFolderPath/AndroidManifest.xml").exists()) {
            File(tmpFolderPath).deleteRecursively()
            File(tmpFilePath).delete()
            return
        }
        // analyze
        val manifest = xmlMapper.readValue(File("$tmpFolderPath/AndroidManifest.xml"), AndroidManifest::class.java)
        val apkTool = parseApkToolYml(tmpFolderPath)

        state.apps[manifest.appId] = (state.apps[manifest.appId] ?: WSApplication(manifest.appId, mutableMapOf()))
            .also {
                it.knownBinaries[hash] = WSApplicationState(
                    Date(),
                    apkTool.versionInfo.versionName,
                    apkTool.versionInfo.versionCode,
                    reporter)
            }
        saveState()
        // cleanup
        File(tmpFolderPath).deleteRecursively()
        val finalFilePath = "$dataPath/apps/${manifest.appId}_${apkTool.versionInfo.versionCode}_$hash.apk"
        if (!File(tmpFilePath).renameTo(File(finalFilePath))) {
            logger.log(Level.WARNING, "Failed to move $tmpFilePath to $finalFilePath.")
        }
    }

    private fun parseApkToolYml(folderPath: String): ApkTool =
        yamlMapper.readValue(File("$folderPath/apktool.yml"), ApkTool::class.java)

    private fun sha256(fileName: String): String {
        val buffer = ByteArray(8 * 1024)
        val digest: MessageDigest = MessageDigest.getInstance("SHA-256")
        File(fileName).inputStream().use { inputStream ->
            var numRead = 0
            while (numRead != -1) {
                numRead = inputStream.read(buffer)
                if (numRead > 0) {
                    digest.update(buffer, 0, numRead)
                }
            }
        }
        return digest.digest().joinToString("") { "%02x".format(it) }
    }

    private fun saveState() {
        File("$dataPath/apps/").mkdirs()
        yamlMapper.writeValue(File("$dataPath/state.yaml"), state)
    }
}


/// our "database"
data class State(
    /**
     * To migrate to a new structure, the version is stored
     */
    val version: Int,
    /**
     * A Map from an appId to an WSApplication
     */
    val apps: MutableMap<String, WSApplication>
)

/**
 * All the information we have about a monitored Application
 */
data class WSApplication(
    val appId: String,
    /**
     * A Map from the hash of an APK to its WSApplicationState
     */
    val knownBinaries: MutableMap<String, WSApplicationState>
)

/**
 * The state of an application binary (APK)
 */
data class WSApplicationState(
    /**
     * The date this APK was first seen
     */
    val date: Date,
    val versionName: String,
    val versionCode: Int,
    /**
     * The app might create a userID. This might be opt-in to not raise privacy concerns but the
     * idea is some kind of gamification, where users get an incentive to be the first to report new
     * binaries.
     */
    val reporterId: String?
)

/// to parse the AndroidManifest.xml files extracted from APKs
@JacksonXmlRootElement(localName = "manifest")
@JsonIgnoreProperties(ignoreUnknown = true)
data class AndroidManifest(
    @JsonProperty("package")
    val appId: String,
    val compileSdkVersion: Int,
    val compileSdkVersionCodename: String
)

/// to parse the
@JsonIgnoreProperties(ignoreUnknown = true)
data class ApkTool(
    val versionInfo: ApkVersionInfo
)

data class ApkVersionInfo(
    val versionName: String,
    val versionCode: Int
)